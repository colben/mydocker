/* Lijc >>>> ---------------- */
/* secure installation */
UPDATE mysql.user SET Password=PASSWORD('ENV_PASS') WHERE User='root';
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
FLUSH PRIVILEGES;
/* create remote root */
CREATE USER root@'%' IDENTIFIED BY 'ENV_PASS';
GRANT ALL ON *.* to root@'%';
GRANT RELOAD ON *.* to root@'%';
FLUSH PRIVILEGES;
/* Lijc <<<< ---------------- */

