/* Create remote user root@% */
SET PASSWORD FOR root@'localhost' = PASSWORD('ENV_PASS');
DROP USER IF EXISTS root@'%';
CREATE USER root@'%' IDENTIFIED BY "ENV_PASS";
GRANT ALL ON *.* to root@'%';
GRANT RELOAD ON *.* to root@'%';
FLUSH PRIVILEGES;
/* ------------------------- */

